﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    public GameObject playerObject;

    public float lookSpeed = 5f;
    public float moveSpeed = 5f;

    public Transform targetLocation;

    List<GameObject> attackPoints = new List<GameObject>();
    public GameObject myAttack;

    // Start is called before the first frame update
    void Start()
    {

        foreach (Transform child in transform)
        {
            if (child.gameObject.CompareTag("AttackPoint"))
            {
                attackPoints.Add(child.gameObject);
                child.GetComponent<MeshRenderer>().enabled = false;
            }
        }

        FindNewTargetLocation();

        Attack();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (targetLocation != null)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetLocation.position, moveSpeed * Time.deltaTime);
        }
        
    }

    void FixedUpdate()
    {
        LookAtPlayer();
    }

    void Attack()
    {
        
        foreach(GameObject attackPoint in attackPoints)
        {
            GameObject attack = (GameObject)Instantiate(myAttack, attackPoint.transform.position, attackPoint.transform.rotation);
            attack.GetComponent<Health>().redShifted = gameObject.GetComponent<Health>().redShifted;
        }


    }

    void FindNewTargetLocation()
    {
        //targetLocation = 
    }

    void LookAtPlayer()
    {
        Vector3 dir = playerObject.transform.position - transform.position;
        Quaternion rot = Quaternion.LookRotation(dir);
        transform.rotation = Quaternion.Slerp(transform.rotation, rot, lookSpeed * Time.deltaTime);
    }
}
