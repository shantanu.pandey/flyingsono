﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{

    //Is this attack redShifted
    //public bool redShifted = false;

    public int damage;

    //splitshot count
    public int splitshot = 0;

    public GameObject myPrefab;

    // Start is called before the first frame update
    void Start()
    {

        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.CompareTag("Enemy") || other.gameObject.CompareTag("EnemyAttack"))
        {
            other.gameObject.GetComponent<Health>().TakeDamage(damage, gameObject.GetComponent<Health>().redShifted);
            SpawnSplitShot();
        }
        else if(!other.gameObject.CompareTag("Player"))
        {
            SpawnSplitShot();
        }

    }

    void SpawnSplitShot()
    {

        List<GameObject> children = new List<GameObject>();

        foreach (Transform child in transform)
        {
            children.Add(child.gameObject);
        }

        for (int i = 0; i < (splitshot * 2); i++)
        {
            GameObject clone = (GameObject)Instantiate(myPrefab, children[i].transform.position, children[i].transform.rotation);
            clone.GetComponent<PlayerAttack>().splitshot = 0;
        }

        Destroy(this.gameObject);
    }

}