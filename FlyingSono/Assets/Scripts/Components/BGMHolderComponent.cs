﻿/*
 * This class just holds all the bgm tracks, not data-driven at all for sho
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Audio
{
    [System.Serializable]
    public struct AudioClipBPM
    {
        public AudioClip m_AudioClip;
        public float m_BPM;
    }

    public class BGMHolderComponent : MonoBehaviour
    {
        public AudioClipBPM[] GetBGMClips()
        {
            return m_GameBGMClips;
        }

        public AudioClipBPM GetBGMClipIndex(int index)
        {
            return m_GameBGMClips[index];
        }

        public AudioClipBPM GetBGMClipName(string inName)
        {
            foreach (AudioClipBPM audioClip in m_GameBGMClips)
            {
                if (audioClip.m_AudioClip.name == inName)
                {
                    return audioClip;
                }
            }
            return m_GameBGMClips[0]; // return the first one because of reasons
        }

        public AudioClipBPM[] m_GameBGMClips;
    }

}