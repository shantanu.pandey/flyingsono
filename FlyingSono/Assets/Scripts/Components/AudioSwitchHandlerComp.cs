﻿/*
 * This class handles changing between different audio tracks;
 * 
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Audio
{
    public class AudioSwitchHandlerComp : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        void SwitchToBGM(string inBGMName)
        {
            AudioClipBPM clipBPM = m_BGMHolder.GetBGMClipName(inBGMName);

            // can we just swap like this?
            // play from the same time?

            m_AudioSource.clip = clipBPM.m_AudioClip; // will this break anything?


            m_AudioChecker.SetBPM(clipBPM.m_BPM);

        }

        public AudioSource m_AudioSource; // the target audio source that we wanna use to change the audio
        public BGMHolderComponent m_BGMHolder; // the holder of all the BGMs
        public AudioEventCheckerComp m_AudioChecker;
    }
}
