﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Audio
{
    [RequireComponent(typeof(AudioSource))]
    public class AudioSourceAnalyzerComp : MonoBehaviour
    {
        // initialize if possible
        private void Awake()
        {
            m_AudioSource = GetComponent<AudioSource>();
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            GetSpectrumData();
        }

        private void GetSpectrumData()
        {
            m_AudioSource.GetSpectrumData(m_Samples, 0, FFTWindow.Blackman);
        }

        public float[] GetSamples()
        {
            return m_Samples;
        }

        // members
        public AudioSource m_AudioSource;
        private float[] m_Samples = new float[512];
    }
}
