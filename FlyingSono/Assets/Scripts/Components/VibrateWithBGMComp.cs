﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Audio
{
    public class VibrateWithBGMComp : MonoBehaviour
    {
        [System.Serializable]
        public struct ChannelBounds
        {
            public int m_upper;
            public int m_lower;
        }

        [System.Serializable]
        public struct ScaleAxes
        {
            public bool m_X;
            public bool m_Y;
            public bool m_Z;
        }

        private void Awake()
        {
            // look for AudioSourceAnalyzerComp in scene
            m_AudioSourceAnalyzerComp = FindObjectOfType<AudioSourceAnalyzerComp>();

            if(m_ChannelBoundsToListenTo.m_lower > m_ChannelBoundsToListenTo.m_upper)
            {
                m_ChannelBoundsToListenTo.m_upper = m_ChannelBoundsToListenTo.m_lower;
            }
        }

        // Start is called before the first frame update
        void Start()
        {
            m_DefaultScale = this.transform.localScale;
        }

        // Update is called once per frame
        void Update()
        {
            // Get Samples from audio source analyzer
            float[] samples = m_AudioSourceAnalyzerComp.GetSamples();
            int lower = m_ChannelBoundsToListenTo.m_lower;
            int upper = m_ChannelBoundsToListenTo.m_upper;
            if(lower < 0)
            {
                lower = 0;
            }
            if(upper >= samples.Length)
            {
                upper = samples.Length - 1;
            }
            float largestVal = 0f;
            for (int i = lower; i < upper; ++i)
            {
                if(samples[i] > largestVal)
                {
                    largestVal = samples[i];
                }
            }

            Vector3 scale = m_DefaultScale;
            if(m_ScaleAxes.m_X)
            {
                scale.x += largestVal * m_ScaleValue;
            }
            if (m_ScaleAxes.m_Y)
            {
                scale.y += largestVal * m_ScaleValue;
            }
            if (m_ScaleAxes.m_Z)
            {
                scale.z += largestVal * m_ScaleValue;
            }
        }

        public ChannelBounds m_ChannelBoundsToListenTo;
        public float m_ScaleValue = 5f;
        public ScaleAxes m_ScaleAxes;

        private AudioSourceAnalyzerComp m_AudioSourceAnalyzerComp;
        private Vector3 m_DefaultScale;
    }

}