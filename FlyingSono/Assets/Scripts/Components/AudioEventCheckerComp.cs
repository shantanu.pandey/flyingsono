﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Check the sampler according to BPM
namespace Audio
{
    public class AudioEventCheckerComp : MonoBehaviour
    {
        // Delegates for others to use and add to
        public delegate void OnBGMHalfBeat(float inTimeSec, float[] inMagnitudes);
        public delegate void OnBGMFullBeat(float inTimeSec, float[] inMagnitudes);

        private void Awake()
        {
            m_secPerHalfBeat = 60f / (m_BPM * 2f);
        }

        // Update is called once per frame
        void Update()
        {
            CheckAudioSourceUpdates();
        }

        public void SetBPM(float inBPM)
        {
            m_BPM = inBPM;
            m_secPerHalfBeat = 60f / (m_BPM * 2f);
        }

        // Getters for delegates
        public OnBGMHalfBeat GetOnBGMHalfBeatDelegate()
        {
            return m_OnBGMHalfBeat;
        }

        public OnBGMFullBeat GetOnBGMFullBeatDelegate()
        {
            return m_OnBGMFullBeat;
        }

        // private functions -----------------------
        private void CheckAudioSourceUpdates()
        {
            // check audio source time?
            float sourceTime = m_AudioSource.time;

            // check if half beat?
        }


        // public fields -----------------------
        public AudioSource m_AudioSource; // the audio source that we should be checking
        public float m_BPM = 120f; // BPM value, hardcoded 120 now

        // private fields -----------------------
        private OnBGMHalfBeat m_OnBGMHalfBeat;
        private OnBGMFullBeat m_OnBGMFullBeat;
        private float m_TimeSinceLastBeat = 0f;
        private float m_secPerHalfBeat = 0f;
    }
}