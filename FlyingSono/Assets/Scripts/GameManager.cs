﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    //Singletons
    //Used for when everything slows down to control the game's speed.
    private static float timeScale = 1;

    //slightly ramps up with each wave this is a multiplier
    private static float difficultyRamp = 1;

    private static GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
    }
}
