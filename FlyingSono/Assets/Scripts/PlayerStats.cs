﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    // Start is called before the first frame update

    public float MaxSpeed { get; set; }
    public float Health { get; set; }
    public float CurrentSpeed { get; set; }
    public State State { get; set; }


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
