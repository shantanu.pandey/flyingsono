﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{

    public int startingHealth = 1;
    private int health;

    public bool redShifted;

    // Start is called before the first frame update
    void Start()
    {
        health = startingHealth;

        if (gameObject.GetComponent<Health>().redShifted && !gameObject.CompareTag("Player"))
        {
            gameObject.GetComponent<Renderer>().material.color = Color.red;
        }
        else
        {
            gameObject.GetComponent<Renderer>().material.color = Color.white;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TakeDamage(int damage, bool attackRedShifted)
    {
        if (redShifted == attackRedShifted)
        {
            health = health - damage;
            if (health < 0 )
            {
                //destroy object
                print("FAdfads");
            }
        }

    }

    public void PlayerTakeDamage(int damage)
    {
        health = health - damage;
        if (health < 0 )
        {
            print("FAdfads");
            //player loses the game
        }
    }

    public void Heal(int healAmount)
    {
        health = health + healAmount;
        if (health >= startingHealth )
        {
            health = startingHealth;
        }
    }
}
