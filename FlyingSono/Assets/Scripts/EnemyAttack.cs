﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{

    public int damage = 1;
    public float missileSpeed = 1;

    // Start is called before the first frame update
    void Start()
    {
        if (gameObject.GetComponent<Health>().redShifted)
        {
            gameObject.GetComponent<Renderer>().material.color = Color.red;
        }
        else
        {
            gameObject.GetComponent<Renderer>().material.color = Color.white;
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += transform.forward * (missileSpeed * Time.deltaTime);
    }

    void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.CompareTag("Player"))
        {
            other.gameObject.GetComponent<Health>().PlayerTakeDamage(damage);
            Destroy(this.gameObject);
        }
        else if (other.gameObject.CompareTag("Environment"))
        {
            Destroy(this.gameObject);
        }
        //else if (other.gameObject.GetTag() == "PlayerAttack" && (redShifted == other.gameObject.GetComponent<PlayerAttack>().redShifted))

    }

}
