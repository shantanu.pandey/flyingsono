﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float Lifetime { get; set; }
    public float m_speed;// { get; set; }

    public Transform firePoint;

    // Start is called before the first frame update
    void Start()
    {
        Lifetime = 5.0f;
        Rigidbody bulletRigidBody = GetComponentInChildren<Rigidbody>();
        //bulletRigidBody.AddForce(firePoint.forward * m_speed, ForceMode.Impulse);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private IEnumerator DestroyBullet()
    {
        yield return new WaitForSeconds(Lifetime);
        Destroy(gameObject);
    }
}
