﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum State
{
    BLACK = 0,
    BLUE = 1,
    RED = 2
}


public class Pickup : MonoBehaviour
{

    public State State { get; set; }
    public float Value { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
