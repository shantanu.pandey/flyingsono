﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class Player : MonoBehaviour
{
    public Transform playerModel;

    public float m_Speed = 10.0f;
    public float m_lookSpeed = 340;

    public int m_damage = 1;

    [Header("Settings")]
    public bool joystick = true;

    public Cinemachine.CinemachineDollyCart dolly;

    public Transform aimTarget;

    public Transform reticule;
    float cinemachineSpeed = 6.0f;

    public PlayerStats m_Stats;

    public CinemachineSmoothPath m_splinePath;


    // Start is called before the first frame update
    void Start()
    {
        //playerModel = transform.GetChild(0);
        var l = m_splinePath.m_Waypoints.Length;
        //Debug.Log(m_splinePath.m_Waypoints[l - 3].position);
        //Vector3 pos = Camera.main.WorldToViewportPoint(m_splinePath.m_Waypoints[l - 3].position);
        //pos.x = Mathf.Clamp01(pos.x);
        //pos.y = Mathf.Clamp01(pos.y);
        //transform.position = Camera.main.ViewportToWorldPoint(pos);
        //transform.position = new Vector3(m_splinePath.m_Waypoints[l - 3].position.x, m_splinePath.m_Waypoints[l - 3].position.y, 0);
    }

    // Update is called once per frame
    void Update()
    {
        float h = joystick ? Input.GetAxis("Horizontal") : Input.GetAxis("Mouse X");
        float v = joystick ? Input.GetAxis("Vertical") : Input.GetAxis("Mouse Y");
                
        LocalMove(h, v, m_Speed);
        RotationLook(h, v, m_lookSpeed);
        HorizontalLean(playerModel, h, 80, .1f);

        if (Input.GetKeyDown(KeyCode.R))
            BarrelRoll(1);

        if (Input.GetKeyDown(KeyCode.U))
            BarrelRoll(-1);

        if (Input.GetKeyDown(KeyCode.Space))
            HitScan();
        //if (Input.GetKeyDown(KeyCode.Space))
        //    HitScan();
    }

    private void LocalMove(float i_x, float i_y, float i_Speed)
    {
        transform.localPosition += new Vector3(i_x, i_y, 0) * i_Speed * Time.deltaTime;
        ClampPosition();
    }

    private void ClampPosition()
    {
        Vector3 pos = Camera.main.WorldToViewportPoint(transform.position);
        pos.x = Mathf.Clamp01(pos.x);
        pos.y = Mathf.Clamp01(pos.y);
        transform.position = Camera.main.ViewportToWorldPoint(pos);
    }

    private void RotationLook(float i_h, float i_v, float i_speed)
    {
        aimTarget.parent.position = Vector3.zero;
        aimTarget.localPosition = new Vector3(i_h, i_v, 1);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(aimTarget.position), Mathf.Deg2Rad * i_speed * Time.deltaTime);
    }

    private void HorizontalLean(Transform target, float axis, float leanLimit, float lerpTime)
    {
        Vector3 targetEulerAngles = target.localEulerAngles;
        target.localEulerAngles = new Vector3(targetEulerAngles.x, targetEulerAngles.y, Mathf.LerpAngle(targetEulerAngles.z, -axis * leanLimit, lerpTime));
    }

    private void BarrelRoll(int dir)
    {
        if (!DOTween.IsTweening(playerModel))
        {
            playerModel.DOLocalRotate(new Vector3(playerModel.localEulerAngles.x, playerModel.localEulerAngles.y, 360 * -dir), .4f, RotateMode.LocalAxisAdd).SetEase(Ease.OutSine);
            playerModel.DOLocalMove(new Vector3(playerModel.localPosition.x - dir * 4, playerModel.localPosition.y, playerModel.localPosition.z), 1);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(reticule.position, .5f);
        Gizmos.DrawSphere(reticule.position, .15f);

    }

    void HitScan()
    {
        RaycastHit hit;
        //GameObject bullet = Instantiate(m_BulletPrefab, m_BulletParent.transform.position, Quaternion.identity) as GameObject;
        Vector3 forward = reticule.transform.TransformDirection(Vector3.forward) * 100;
        Debug.DrawRay(reticule.transform.position, forward, Color.green);
        if (Physics.Raycast(transform.position, forward, out hit))
        {
            //Debug.Log(hit.distance);
            if (hit.collider.gameObject.tag.Contains("Enemy"))
                Debug.Log("enemy hit");
        }
    }



}
