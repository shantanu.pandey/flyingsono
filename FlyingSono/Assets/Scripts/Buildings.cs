﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Buildings : MonoBehaviour
{
    public int damage = 10;

    Renderer buildingColor;

    new Color fadedBuildingColor;

    public float buildingFadeTime = 1;

    // Start is called before the first frame update
    void Start()
    {
        buildingColor = gameObject.GetComponent<Renderer>();
        //.material.color
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        print("fasfs");
        if (other.gameObject.CompareTag("Player"))
        {
            other.gameObject.GetComponent<Health>().PlayerTakeDamage(damage);
            gameObject.GetComponent<BoxCollider>().enabled = false;
            BuildingFadeOut();
        }
    }

    void BuildingFadeOut()
    {
        //buildingColor.material.color = new Color(buildingColor.material.color.r, buildingColor.material.color.g, buildingColor.material.color.b, 0f);
        fadedBuildingColor = new Color(buildingColor.material.color.r, buildingColor.material.color.g, buildingColor.material.color.b, 0f);

        StartCoroutine(DoAThingOverTime( buildingColor.material.color, fadedBuildingColor, buildingFadeTime) );
        ;
    }

    IEnumerator DoAThingOverTime(Color start, Color end, float duration) 
    {
        for (float t=0f; t<duration; t+=Time.deltaTime) 
        {
            float normalizedTime = t/duration;
            //right here, you can now use normalizedTime as the third parameter in any Lerp from start to end
            buildingColor.material.color = Color.Lerp(start, end, normalizedTime);
            yield return null;
        }
        buildingColor.material.color = end; //without this, the value will end at something like 0.9992367
    }
}
